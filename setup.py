from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()


setup(
    name='webserver',
    version='0.1.0',
    description='Simple web server app with endpoints for key value pair manipulation',
    long_description=readme,
    author='Marko Kos',
    author_email='marko.kos89@gmail.com',
    url='http://localhost:5000',
    install_requires=[
        'autopep8==1.6.0',
        'Flask==2.0.2',
        'Flask-HTTPAuth==4.5.0',
        'Flask-SQLAlchemy==2.5.1'
    ]
)
