# Webserver

Webserver app is a simple app for key value pair manipulation.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install webserver.

```bash
pip install .
```

or for development install requirements from requirements.txt file and run main.py

```bash
pip install requirements.txt
```

## Usage

The app has three exposed endpoints.

/api/user
/api/key
/api/keys

The /api/user endpoints creates a user that is used for basic authentication. Must be created to use the /api/key endpoint.

The /api/key endpoint does all the manipulaton of key value pairs. Supports fetching a key (GET), inserting a key (POST), updating a key (PUT) and removing a key (DELETE). The endpoints require basic authentication.

The /api/keys endpoints fetches all keys from the database. Basic authentication is not required.

## Bulit with

* [Python](https://www.python.org/) - Python programming language
* [Flask](https://flask.palletsprojects.com/) - Flask framework
