from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth

db = SQLAlchemy()
basic_auth = HTTPBasicAuth()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'some_secret_key_123_456_789'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'

    db.init_app(app)

    # auth routes blueprint
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # non-auth routes blueprint
    from .views import views as views_blueprint
    app.register_blueprint(views_blueprint)

    return app
