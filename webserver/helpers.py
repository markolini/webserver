from flask import Response
from .models import KeyValuePair


def make_response(response_payload, status_code):
    return Response(response_payload, status=status_code, mimetype='application/json')


def fetch_key_value_pair(key):
    return KeyValuePair.query.filter_by(key=key).first()
