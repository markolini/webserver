from flask import Blueprint, request
from .constants import ResponseCodes
from .helpers import make_response, fetch_key_value_pair
from .models import KeyValuePair, PairResponse
from . import db, basic_auth

import json

views = Blueprint('views', __name__)


@views.route('/api/key', methods=['GET', 'POST', 'PUT', 'DELETE'])
@basic_auth.login_required
def key():
    if request.method == 'GET':
        key = request.args.get('key')
        key_value_pair = fetch_key_value_pair(key)

        if key_value_pair is None:
            return make_response(f'Key {key} does not exist', ResponseCodes.NOT_FOUND.value)

        return make_response(key_value_pair.value, ResponseCodes.SUCCESS.value)

    if request.method == 'POST':
        key = request.json.get('key')
        value = request.json.get('value')

        key_value_pair = fetch_key_value_pair(key)
        if key_value_pair is not None:
            return make_response(f'Key {key} already exists', ResponseCodes.BAD_REQUEST.value)

        pair = KeyValuePair(key=key, value=value)
        db.session.add(pair)
        db.session.commit()

        return make_response(f'New key created {key}', ResponseCodes.SUCCESS.value)

    if request.method == 'PUT':
        key = request.json.get('key')
        value = request.json.get('value')

        key_value_pair = fetch_key_value_pair(key)
        if key_value_pair is None:
            return make_response(f'Key {key} does not exist', ResponseCodes.NOT_FOUND.value)

        key_value_pair.value = value
        db.session.commit()

        return make_response(f'Succesfully updated value of a key {key}', ResponseCodes.SUCCESS.value)

    if request.method == 'DELETE':
        key = request.args.get('key')

        key_value_pair = fetch_key_value_pair(key)
        if key_value_pair is None:
            return make_response(f'Key {key} does not exist', ResponseCodes.NOT_FOUND.value)

        db.session.delete(key_value_pair)
        db.session.commit()

        return make_response(f'Deleted key {key}', ResponseCodes.SUCCESS.value)


@views.route('/api/keys', methods=['GET'])
def fetch_keys():

    keys = KeyValuePair.query.all()
    if keys is None:
        return make_response('No keys found', ResponseCodes.NOT_FOUND.value)

    response_keys = []
    for key in keys:
        response_keys.append(PairResponse(key.key, key.value).__dict__)

    json_response = json.dumps(response_keys)

    return make_response(json_response, ResponseCodes.SUCCESS.value)
