from re import M
from flask import Blueprint, request, Response, g
from werkzeug.security import generate_password_hash, check_password_hash
from .models import User
from . import db, basic_auth
from .helpers import make_response
from .constants import ResponseCodes

auth = Blueprint('auth', __name__)


@auth.route('/api/user', methods=['POST'])
def create_user():
    user = request.json.get('username')
    password = request.json.get('password')

    if user is None or password is None:
        return make_response('Username or password is empty', ResponseCodes.BAD_REQUEST.value)

    if User.query.filter_by(username=user).first() is not None:
        return make_response('Username is already taken', ResponseCodes.BAD_REQUEST.value)

    user = User(username=user, password=generate_password_hash(
        password, method='sha256'))
    db.session.add(user)
    db.session.commit()

    return make_response('User created', ResponseCodes.SUCCESS.value)


@basic_auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()
    if not user or not check_password_hash(user.password, password):
        return False
    g.user = user
    return True
